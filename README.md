# gitlab-cloud-runner

An on-prem runner for Gitlab Cloud

# Installation

Run the below to launch your runner:
```
docker-compose up -d
```

# Config

Run the following to run a temporary registration container

```
docker run --rm -it -v $(pwd)/config:/etc/gitlab-runner gitlab/gitlab-runner register
```