FROM gitlab/gitlab-runner:latest

RUN apt-get update && \
    apt-get install -y \
        jq \
        docker.io